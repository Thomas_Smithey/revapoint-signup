const express = require("express");
const bunyan = require("bunyan");
const {LoggingBunyan} = require("@google-cloud/logging-bunyan");
const cloudLogger = new LoggingBunyan();

const app = express();

// logs come in levels
// trace
// debug
// info
// warn
// error
// fatal

const config =  // for bunyan
{
    name: "signup-app",
    // stream are where you log the data
    streams:
    [
        {
            stream:process.stdout,
            level: "info"
        },
        {
            path: "signup.log",
            type: "file"
        },
        cloudLogger.stream("info")
    ]
};

const logger = bunyan.createLogger(config);

app.get("/signup/:email", (req, res) =>
{
    const email = req.params.email;
    if (email.includes("@"))
    {
        logger.info(`A new person registered with email ${email}.`)
        res.status(200).send(`Your email ${email} is being processed.`);
    }
    else
    {
        logger.warn("A person tried to use an invalid email.");
        res.status(422).send("That is an invalid email.");
    }
});

app.listen(3000, ()=>console.log("Application started"));